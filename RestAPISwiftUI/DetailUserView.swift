//
//  DetailUserView.swift
//  RestAPISwiftUI
//
//  Created by Salvador Lopez on 09/06/23.
//

import SwiftUI

struct DetailUserView: View{
    
    let user: User
    
    var body: some View{
        VStack{
            ZStack{
                Color.blue
                    .frame(width: 210,height: 210)
                    .cornerRadius(110)
                Image("tux")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 200, height: 200)
                    .background(.red)
                    .cornerRadius(100)
            }
            VStack{
                Text(user.name!)
                    .font(.largeTitle)
                    .fontWeight(.ultraLight)
                    .foregroundColor(.blue)
                Text(user.email!)
                    .font(.body)
                    .fontWeight(.thin)
            }
            VStack{
                TextField("Name", text: .constant(user.name!))
                    .textFieldStyle(.roundedBorder)
                TextField("Username", text: .constant(user.username!))
                    .textFieldStyle(.roundedBorder)
                TextField("Correo Electronico", text: .constant(user.email!))
                    .textFieldStyle(.roundedBorder)
                TextField("Phone", text: .constant(user.phone!))
                    .textFieldStyle(.roundedBorder)
                TextField("Website", text: .constant(user.website!))
                    .textFieldStyle(.roundedBorder)
            }
            .padding()
            Button{
                //Save this
            } label: {
                Text("Guardar")
                    .frame(width: 100, alignment: .center)
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.blue)
                    .cornerRadius(50)
            }
        }
    }
    
}

struct DetailUserView_Previews: PreviewProvider {
    static var previews: some View {
        let user = User(id: 1, name: "Name", username: "Username", email: "email", address: Address(street: "Street", suite: "Suite", city: "City"), phone: "phone", website: "website", company: Company(name: "Company", catchPhrase: "Phrase", bs: "Business"))
        DetailUserView(user: user)
    }
}
