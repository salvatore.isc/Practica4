//
//  RestAPISwiftUIApp.swift
//  RestAPISwiftUI
//
//  Created by Salvador Lopez on 09/06/23.
//

import SwiftUI

@main
struct RestAPISwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
