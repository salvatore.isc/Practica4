//
//  DataModel.swift
//  RestAPISwiftUI
//
//  Created by Salvador Lopez on 09/06/23.
//

import Foundation

func getData(_ url:String, completion: @escaping ([User]) -> Void) {
    
    var request = URLRequest(url: URL(string: url)!)
    request.httpMethod = "GET"
    
    let task = URLSession.shared.dataTask(with: request){
        data, response, error in
    
        if let error = error {
            print("Error: \(error)")
        }else{
            do{
                let fetchedUsers = try JSONDecoder().decode([User].self, from: data!)
                completion(fetchedUsers)
            }catch{
                print("Error: \(error)")
            }
        }
        
    }
    task.resume()
    
}
